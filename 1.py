# Завдання 1
# Створіть функцію, яка приймає список з елементів типу int, а повертає новий список з рядкових
# значень вихідного масиву. Додайте анотацію типів для вхідних і вислідних
# значень функції.

from typing import List


def generate_list(input_list: List[int]) -> List[str]:
    """
    Converts a list of integers to a list of string values.

    :param input_list: Input list of elements of type int.
    :type input_list: List[int]
    :return: A new list with a string value.
    :rtype: List[str]
    """
    result = []
    for item in input_list:
        result.append(str(item))
    return result


test_data1 = [10, 20, 30, 40, 50]

test_value1 = generate_list(test_data1)

print(test_value1)
