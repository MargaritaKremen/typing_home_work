# Завдання 2
# Створіть два класи Directory (тека) і File (файл) з типами (анотацією).
# Клас Directory має мати такі поля:
# ·        назва (name типу str);
# ·        батьківська тека (root типу Directory);
# ·        список файлів (список типу files, який складається з екземплярів File);
# ·        список підтек (список типу sub_directories, який складається з екземплярів Directory).
#
# Клас Directory має мати такі поля:
# ·        додавання теки до списку підтек (add_sub_directory, який приймає екземпляр Directory та присвоює поле root для приймального екземпляра);
# ·        видалення теки зі списку підтек (remove_sub_directory, який приймає екземпляр Directory та обнуляє поле root. Метод також видаляє теку зі списку sub_directories);
# ·        додавання файлу в теку (add_file, який приймає екземпляр File і присвоює йому поле directory – див. клас File нижче);
# ·        видалення файлу з теки (remove_file, який приймає екземпляр File та обнуляє у нього поле directory. Метод видаляє файл зі списку files).
#
# Клас File має мати такі поля:
# ·        назва (name типу str);
# ·        тека (Directory типу Directory).


from typing import List, Optional
from pathlib import Path


class Directory:
    def __init__(self, name: str, root: Optional["Directory"] = None):
        """
        Initializes an object of the Directory class.
        :param name: Folder name.
        :type name: str
        :param root: Parent folder (default None).
        :type root: Optional[Directory]
        """
        self.name: str = name
        self.root: Optional["Directory"] = root
        self.files: List["File"] = []
        self.sub_directories: List["Directory"] = []

    def add_sub_directory(self, sub_directory: "Directory"):
        """
        Adds a subdirectory to the list of subdirectories of this directory.

        :param sub_directory: An instance of the Directory class to add.
        :type sub_directory: Directory
        """
        sub_directory.root = self
        self.sub_directories.append(sub_directory)

    def remove_sub_directory(self, sub_directory: "Directory"):
        """
        Removes a subdirectory from the list of subdirectories of this directory.
        :param sub_directory: An instance of the Directory class to delete.
        :type sub_directory: Directory
        """
        sub_directory.root = None
        self.sub_directories.remove(sub_directory)

    def add_file(self, file: "File"):
        """
        Adds a file to the list of files in this folder.

        :param file: An instance of the File class to add.
        :type file: File
        """
        file.directory = self
        self.files.append(file)

    def remove_file(self, file: "File"):
        """
        Removes a file from this folder's file list.

        :param file: An instance of the File class to delete.
        :type file: File
        """
        file.directory = None
        self.files.remove(file)

    def create(self):
        """
        Creates a folder in the file system.

        """
        # Define the path to this folder:
        directory_path = Path(self.get_full_path())

        # Create the folder:
        directory_path.mkdir(parents=True, exist_ok=True)

    def delete(self):
        """
        Removes a folder and its contents from the file system.
        """
        # Define the path to this folder:
        directory_path = Path(self.get_full_path())

        # Delete the folder and its contents:
        directory_path.rmdir()

    def get_full_path(self):
        """
        Gets the full path to the folder.

        :return: full path to the folder.
        :rtype: str
        """
        if self.root:
            return Path(self.root.get_full_path()) / self.name
        return self.name


class File:
    def __init__(self, name: str, directory: Optional["Directory"] = None):
        """
        Initializes an object of the File class.

        :param name: The name of the file.
        :type name: str
        :param directory: The folder in which the file is located (default None).
        :type directory: Optional[Directory]
        """
        self.name: str = name
        self.directory: Optional[Directory] = directory

    def create(self):
        """
        Creates a file in the file system.
        """

        # Determine the path to this file:
        file_path = Path(self.get_full_path())

        # Create an empty file:
        file_path.touch()

    def delete(self):
        """
        * Deletes a file from the file system.
        """
        # Determine the path to this file:
        file_path = Path(self.get_full_path())

        # Delete the file
        file_path.unlink()

    def get_full_path(self):
        """
        Gets the full path to the file.

        :return: The full path to the file.
        :rtype: str
        """
        if self.directory:
            return Path(self.directory.get_full_path()) / self.name
        return self.name


# Example:
# Creating the root directory:
root_directory = Directory("my_root_directory")
root_directory.create()

# Create a subdirectory in the root directory
sub_directory = Directory("sub_dir1", root_directory)
sub_directory.create()

# Create a file in a subdirectory
file = File("file1.txt", sub_directory)
file.create()

# # Delete the file
# file.delete()
#
# # Delete the subdirectory
# sub_directory.delete()
#
# # Delete the root directory
# root_directory.delete()
