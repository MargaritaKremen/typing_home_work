# Завдання 3
# Використовуючи модуль sqlite3 та модуль smtplib, реалізуйте реальне додавання користувачів до бази.
# Мають бути реалізовані такі функції та класи:
# ·        клас користувача, що містить у собі такі методи: get_full_name (ПІБ з поділом через пробіл:
#                    «Петров Ігор Сергійович»), get_short_name (формату ПІБ: «Петров І. С.»), get_age
#                    (повертає вік користувача, використовуючи поле birthday типу datetime.date); метод __str__
#                    (повертає ПІБ та дату народження);
# ·        функція реєстрації нового користувача (приймаємо екземпляр нового користувача та відправляємо email на пошту
#                    користувача з листом подяки).
# ·        функція відправлення email з листом подяки.
# ·        функція пошуку користувачів у таблиці users за іменем, прізвищем і поштою.
#
# Протестувати цей функціонал, використовуючи заглушки у місцях надсилання пошти.
# Під час штатного запуску програми вона має відправляти повідомлення на вашу реальну поштову скриньку
# (необхідно налаштувати SMTP, використовуючи доступи від провайдера вашого email-сервісу).


import sqlite3
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import datetime
from typing import List, Tuple


# Перевірка наявності таблиці users в базі даних
def check_users_table_exists() -> bool:
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='users'")
    exists = cursor.fetchone() is not None
    conn.close()
    return exists


# Створення таблиці users, якщо вона не існує
if not check_users_table_exists():
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            first_name TEXT NOT NULL,
            last_name TEXT NOT NULL,
            middle_name TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE,
            birthday DATE NOT NULL
        )
    ''')
    conn.commit()
    conn.close()


# Клас користувача
class User:
    def __init__(self, first_name: str, last_name: str, middle_name: str, email: str, birthday: datetime.date):
        self.first_name = first_name
        self.last_name = last_name
        self.middle_name = middle_name
        self.email = email
        self.birthday = birthday

    def get_full_name(self) -> str:
        return f"{self.last_name} {self.first_name} {self.middle_name}"

    def get_short_name(self) -> str:
        name_parts = self.first_name.split()
        middle_name_parts = self.middle_name.split()
        initials_first = [name[0] + '.' for name in name_parts]
        initials_middle = [name[0] + '.' for name in middle_name_parts]
        return f"{self.last_name} {' '.join(initials_first)} {' '.join(initials_middle)}"

    def get_age(self) -> str:
        # Вираховуємо вік користувача
        today = datetime.date.today()
        age = today.year - self.birthday.year - \
            ((today.month, today.day) < (self.birthday.month, self.birthday.day))
        return age

    def __str__(self) -> str:
        return f"{self.get_full_name()} {self.birthday} {self.email}"


# Функція реєстрації нового користувача
def register_user(user: User) -> None:
    # Додати користувача до бази даних SQLite
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute(
        "SELECT id, first_name, email FROM users WHERE email = ? OR (first_name = ? AND last_name = ? \
        AND middle_name = ? AND birthday = ?)",
        (user.email, user.first_name, user.last_name, user.middle_name, user.birthday))
    existing_user = cursor.fetchone()

    if existing_user:
        cursor.execute(
            "SELECT id, first_name, email FROM users WHERE email = ? OR (first_name = ? AND last_name = ? \
            AND middle_name = ? AND birthday = ?)",
            (user.email, user.first_name, user.last_name, user.middle_name, user.birthday))
    else:
        cursor.execute(
            "INSERT INTO users (first_name, last_name, middle_name, email, birthday) VALUES (?, ?, ?, ?, ?)",
            (user.first_name,
             user.last_name,
             user.middle_name,
             user.email,
             user.birthday))
        # existing_user = cursor.fetchone()
        conn.commit()
        conn.close()

        # Відправка листа подяки
        send_thank_you_email(user)


# Функція відправки email з листом подяки
def send_thank_you_email(user)-> None:
    # Налаштовую SMTP
    smtp_server = 'smtp.gmail.com'
    smtp_port = 587
    smtp_username = 'kozlovamargarita1616@gmail.com'
    smtp_password = 'traven10'

    # об'єкт SMTP
    smtp = smtplib.SMTP(smtp_server, smtp_port)
    smtp.starttls()

    try:
        smtp.login(smtp_username, 'ibqd jvse psuf ngfy')

        # лист подяки
        subject = 'Welcome!'
        from_email = 'kozlovamargarita1616@gmail.com'
        to_email = user.email
        message = MIMEMultipart()
        message['From'] = from_email
        message['To'] = to_email
        message['Subject'] = subject

        body = f"Thank you for registering, {user.get_full_name()}!\n"
        f'We are happy to welcome you to our store.'

        message.attach(MIMEText(body, 'plain'))

        # Відправляю лист
        smtp.sendmail(from_email, to_email, message.as_string())

        # Завершую з'єднання
        smtp.quit()

        print(f"Лист подяки відправлено на {to_email}")

    except Exception as e:
        print(f"Помилка відправлення листа: {str(e)}")


# Функція пошуку користувачів у таблиці users за іменем, прізвищем і поштою
def search_users(query: str) -> List[Tuple[int, str, str, str, str, datetime.date]]:
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute(
        "SELECT * FROM users WHERE (first_name || last_name || middle_name || birthday) LIKE ? OR email LIKE ?",
        (f"%{query}%",
         f"%{query}%"))
    users = cursor.fetchall()
    conn.close()
    return users


if __name__ == "__main__":
    # Тестування функціоналу
    new_user = User(
        "Ігор",
        "Петров",
        "Сергійович",
        "margo197721@gmail.com",
        datetime.date(
            1990,
            9,
            23))

    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute(
        "SELECT id, first_name, email FROM users WHERE email = ? OR (first_name = ? AND last_name = ? \
        AND middle_name = ? AND birthday = ?)",
        (new_user.email, new_user.first_name, new_user.last_name, new_user.middle_name, new_user.birthday))
    existing_user = cursor.fetchone()

    if existing_user:
        if existing_user[2] == new_user.email:
            print(f"Вже є користувач з електронною поштою {new_user.email}")
        else:
            print(f"Вже є користувач з ім'ям {new_user.first_name}")
    else:
        register_user(new_user)

    cursor.execute("SELECT * FROM users")
    existing_users = cursor.fetchall()
    conn.close()

    print("\nПерелік користувачів:")
    for user_data in existing_users:
        user = User(
            user_data[1],
            user_data[2],
            user_data[3],
            user_data[4],
            user_data[5])
        print(user)
